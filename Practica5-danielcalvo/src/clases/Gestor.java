package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class Gestor {

	private ArrayList<Cubito> listaCubitos;
	private ArrayList<Usuario> listaUsuario;
	
	public Gestor() {
		listaCubitos=new ArrayList<Cubito>();
		listaUsuario=new ArrayList<Usuario>();
	}

	public boolean existeUsuario(String dni) {
		for(Usuario usuario : listaUsuario) {
			if(usuario!=null && usuario.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}
	
	public void altaUsuario(String dni, String nombre, double saldo, String fechaCompra) {
		if(!existeUsuario(dni)) {
			Usuario nuevoUsuario = new Usuario(dni, nombre);
			nuevoUsuario.setSaldo(saldo);
			listaUsuario.add(nuevoUsuario);
		} else {
			System.out.println("El usuario ya existe");
		}
	}
	
	public void listarUsuario() {
		for(Usuario usuario : listaUsuario) {
			if(usuario != null) {
				System.out.println(usuario);
			}
		}
	}
	
	public Usuario buscarUsuario(String dni) {
		for(Usuario usuario : listaUsuario) {
			if(usuario!=null && usuario.getDni().equals(dni)) {
				return usuario;
			}
		}
		return null;
	}
	
	public void eliminarUsuario(String dni) {
		Iterator<Usuario> iteradorUsuarios = listaUsuario.iterator();
		while(iteradorUsuarios.hasNext()) {
			Usuario usuario=iteradorUsuarios.next();
			if(usuario.getDni().equals(dni)) {
				iteradorUsuarios.remove();
			}
		}
	}
	
	public boolean existeCubito(int idCubito) {
		for(Cubito cubito : listaCubitos) {
			if(cubito!=null && cubito.getIdCubito()==idCubito) {
				return true;
			}
		}
		return false;
	}
	
	public void altaCubito(int idCubito, String marca, String modelo, double precio) {
		if(!existeCubito(idCubito)) {
		Cubito nuevoCubito = new Cubito(idCubito);
		nuevoCubito.setMarca(marca);
		nuevoCubito.setModelo(modelo);
		nuevoCubito.setPrecio(precio);
		listaCubitos.add(nuevoCubito);
		} else {
			System.out.println("El cubito ya existe");
		}
	}
	
	public void eliminarCubito(int idCubito) {
		Iterator<Cubito> iteradorCubitos = listaCubitos.iterator();
		while(iteradorCubitos.hasNext()) {
			Cubito cubito=iteradorCubitos.next();
			if(cubito.getIdCubito()==idCubito) {
				iteradorCubitos.remove();
			}
		}
	}
	
	public Cubito buscarCubito(int idCubito) {
		for(Cubito cubito : listaCubitos) {
			if(cubito!=null && cubito.getIdCubito()==idCubito) {
				return cubito;
			}
		}
		return null;
	}
	
	public void listarCubitosPorMarca(String marca) {
		for(Cubito cubito : listaCubitos) {
			if(cubito != null && cubito.getMarca().equals(marca)) {
				System.out.println(cubito);
			}
		}
	}
	
	
	public void asignarUsuario(String dni, int idCubito) {
		if(buscarUsuario(dni)!=null && buscarCubito(idCubito)!=null) {
			Usuario user = buscarUsuario(dni);
			Cubito cubito = buscarCubito(idCubito);
			cubito.setCubitoUsuario(user);
		}
	}
	
	
	public void listarCubitosCompradorPorUsuario(String dni) {
		for(Cubito cubito: listaCubitos) {
			if(cubito.getCubitoUsuario()!=null && cubito.getCubitoUsuario().getDni().equals(dni)) {
				System.out.println(cubito);
			}
		}
	}
	
	public void usuarioVIP(String dni, String nombre) {
		for(Usuario user : listaUsuario) {
			if(user.getDni()!=null && user.getDni().equals(dni)) {
				user.setNombre(nombre+"*");
			}
		}
	}
	
	public boolean esVIP(String dni, String nombre) {
		String vip="*";
		for(Usuario user: listaUsuario) {
			if(user.getDni()!=null && user.getDni().equals(dni)) {
				user.getNombre().compareTo(vip);
			}
		}
		return false;	
	}
	
	public void descuentos(String dni, String nombre, int idCubito, double precio) {
		if(esVIP(dni, nombre)) {
		for(Cubito cubito : listaCubitos) {
			if(cubito.getIdCubito()!=0 && cubito.getIdCubito()==idCubito) {
				cubito.setPrecio(precio*0.80);
			} else {
				System.out.println("El cubito no existe");
			}
		}
		}
	}
	
	public void comprar(String dni, double saldo,int idCubito, double precio) {
		if(saldo>precio) {
		for(Usuario user : listaUsuario) {
			user.setSaldo(saldo-precio);
		}
		} else {
			System.out.println("No tienes suficiente saldo");
		}
	}
}
