package clases;


public class Cubito {

	private int idCubito;
	private String marca;
	private String modelo;
	private double precio;

	private Usuario cubitoUsuario;
	
	public Cubito(int idCubito) {
		this.idCubito=idCubito;
	}

	public int getIdCubito() {
		return idCubito;
	}

	public void setIdCubito(int idCubito) {
		this.idCubito = idCubito;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Usuario getCubitoUsuario() {
		return cubitoUsuario;
	}

	public void setCubitoUsuario(Usuario cubitoUsuario) {
		this.cubitoUsuario = cubitoUsuario;
	}
	

	@Override
	public String toString() {
		return "Cubito [idCubito=" + idCubito + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cubitoUsuario=" + cubitoUsuario + "]";
	}

	

}
