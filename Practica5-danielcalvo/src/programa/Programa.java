package programa;

import java.util.Scanner;

import clases.Gestor;

public class Programa {
	
	public static void main(String[] args) {
		Scanner lector=new Scanner(System.in);
		System.out.println("Creo instancia");
		Gestor gestor = new Gestor();
		int opcion;
		do {
			System.out.println("~~MENU~~");
			System.out.println("Elige una opcion de estas, para terminar pulse 0");
			System.out.println("1. Dar de alta usuario");
			System.out.println("2. Listar usuarios");
			System.out.println("3. Buscar usuario");
			System.out.println("4. Eliminar un usuario");
			System.out.println("5. Dar de alta un cubito");
			System.out.println("6. Buscar cubito");
			System.out.println("7. Eliminar cubito");
			System.out.println("8. Listar cubitos por marca");
			System.out.println("9. Asociar usuario a cubito");
			System.out.println("10. Comprar cubitos");
			System.out.println("11. Hacer vip");
			System.out.println("12. Hacer descuento");
			opcion=lector.nextInt();
			
			switch(opcion) {
			
			case 0: 
				break;
				
			case 1:
				lector.nextLine();
				String dni;
				String nombre;
				double saldo;
				String fechaCompra = null;
				System.out.println("Has seleccionado dar de alta un usuario");
				System.out.println("Dame su DNI");
				dni=lector.nextLine();
				System.out.println("Dame su nombre");
				nombre=lector.nextLine();
				System.out.println("Dame su saldo");
				saldo=lector.nextDouble();
				lector.nextLine();
				System.out.println("Dame la fecha de compra");
				fechaCompra=lector.nextLine();
				gestor.altaUsuario(dni, nombre, saldo, fechaCompra);
				break;
			
			case 2:
				System.out.println("Has seleccionado listar usuarios");
				gestor.listarUsuario();
				break;
				
			case 3:
				lector.nextLine();
				System.out.println("Has seleccionado buscar un usuario");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				gestor.buscarUsuario(dni);
				break;
				
			case 4:
				lector.nextLine();
				System.out.println("Has seleccionado eliminar un usuario");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				gestor.eliminarUsuario(dni);
				break;
				
			case 5:
				lector.nextLine();
				int idCubito;
				String marca;
				String modelo;
				double precio;
				System.out.println("Has seleccionado dar de alta un cubito");
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				lector.nextLine();
				System.out.println("Dame su marca");
				marca=lector.nextLine();
				System.out.println("Dame su modelo");
				modelo=lector.nextLine();
				System.out.println("Dame su precio");
				precio=lector.nextDouble();
				gestor.altaCubito(idCubito, marca, modelo, precio);
				break;
				
			case 6:
				lector.nextLine();
				System.out.println("Has seleccionado buscar un cubito");
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				gestor.buscarCubito(idCubito);
				break;
				
			case 7:
				lector.nextLine();
				System.out.println("Has seleccionado eliminar un cubito");
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				gestor.eliminarCubito(idCubito);
				break;
				
			case 8:
				lector.nextLine();
				System.out.println("Has seleccionado listar cubitos por marca");
				System.out.println("Dame su marca");
				marca=lector.nextLine();
				gestor.listarCubitosPorMarca(marca);
				break;
				
			case 9:
				lector.nextLine();
				System.out.println("Has seleccionado asociar cubito a usuario");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				gestor.asignarUsuario(dni, idCubito);
				break;
				
			case 10:
				lector.nextLine();
				System.out.println("Has seleccionado comprar cubitos");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				System.out.println("Dame su saldo");
				saldo=lector.nextDouble();
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				System.out.println("Dame su precio");
				precio=lector.nextDouble();
				gestor.comprar(dni, saldo, idCubito, precio);
				break;
				
			case 11:
				lector.nextLine();
				System.out.println("Has seleccionado hacer VIP");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				System.out.println("Dame su nombre");
				nombre=lector.nextLine();
				gestor.usuarioVIP(dni, nombre);
				break;
				
			case 12:
				lector.nextLine();
				System.out.println("Has seleccionado hacer descuento");
				System.out.println("Dame su dni");
				dni=lector.nextLine();
				System.out.println("Dame su nombre");
				nombre=lector.nextLine();
				System.out.println("Dame su id");
				idCubito=lector.nextInt();
				System.out.println("Dame su precio");
				precio=lector.nextDouble();
				gestor.descuentos(dni, nombre, idCubito, precio);
				break;
				
			default:
				System.out.println("Opci�n incorrecta, prueba a poner un numero del 1 al 12.");
				break;
			}
		}while(opcion!=0);
		System.out.println("Adi�s!");
		lector.close();
	}
}
